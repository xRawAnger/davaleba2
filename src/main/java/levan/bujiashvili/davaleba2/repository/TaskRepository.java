package levan.bujiashvili.davaleba2.repository;

import levan.bujiashvili.davaleba2.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task,Long> {
}
