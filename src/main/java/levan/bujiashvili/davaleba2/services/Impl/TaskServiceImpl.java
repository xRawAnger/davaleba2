package levan.bujiashvili.davaleba2.services.Impl;

import levan.bujiashvili.davaleba2.dto.task.*;
import levan.bujiashvili.davaleba2.model.Task;
import levan.bujiashvili.davaleba2.repository.TaskRepository;
import levan.bujiashvili.davaleba2.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class TaskServiceImpl implements TaskService {
    @Autowired
    TaskRepository taskRepository;


    @Override
    public GetTasksOutput getTask(GetTasksInput getTasksInput) {
        List<TaskDTO> taskDTOList = new ArrayList<>();
        for (Task task : taskRepository.findAll()) {
            TaskDTO taskDTO = new TaskDTO();
            taskDTO.setId(task.getId());
            taskDTO.setTitle(task.getTitle());
            taskDTO.setDescription(task.getDescription());
            taskDTO.setStartDate(task.getStartDate());
            taskDTO.setEndDate(task.getEndDate());
            taskDTOList.add(taskDTO);
        }
        GetTasksOutput getTasksOutput = new GetTasksOutput();
        getTasksOutput.setTasks(taskDTOList);
        return getTasksOutput;
    }

    @Override
    public AddTaskOutput addTask(AddTaskInput addTaskInput) {
            Task task = new Task();
            task.setTitle(addTaskInput.getTitle());
            task.setDescription(addTaskInput.getDescription());
            task.setStartDate(addTaskInput.getStartDate());
            task.setEndDate(addTaskInput.getEndDate());

            taskRepository.save(task);

            AddTaskOutput addTaskOutput = new AddTaskOutput();
            addTaskOutput.setMsg("ტასკი დაემატა");
            return addTaskOutput;
    }

    @Override
    public DeleteTaskOutput deleteTask(DeleteTaskInput deleteTaskInput) {
        Task task = taskRepository.getOne(deleteTaskInput.getTaskId());
        taskRepository.delete(task);

        DeleteTaskOutput deleteTaskOutput = new DeleteTaskOutput();
        deleteTaskOutput.setMsg("ტასკი წარმატებით წაიშალა");
        return deleteTaskOutput;
    }
}
