package levan.bujiashvili.davaleba2.services;

import levan.bujiashvili.davaleba2.dto.task.*;

public interface TaskService {
    // ტასკის წამოღება
    GetTasksOutput getTask(GetTasksInput getTasksInput);

    // ტასკის დამატება
    AddTaskOutput addTask(AddTaskInput addTaskInput);

    //ტასკის წაშლა
    DeleteTaskOutput deleteTask(DeleteTaskInput deleteTaskInput);
}
