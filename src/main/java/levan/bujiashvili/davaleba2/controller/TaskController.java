package levan.bujiashvili.davaleba2.controller;

import levan.bujiashvili.davaleba2.dto.task.*;
import levan.bujiashvili.davaleba2.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/TaskController")
public class TaskController {

    @Autowired
    TaskService taskService;

    @PostMapping("getTasks")
    public GetTasksOutput getPosts(@RequestBody GetTasksInput getTasksInput) {
        return taskService.getTask(getTasksInput);
    }

    @PostMapping("addTask")
    public AddTaskOutput addPost(@RequestBody AddTaskInput addTaskInput) {
        return taskService.addTask(addTaskInput);
    }


    @PostMapping("deleteTask")
    public DeleteTaskOutput deletePost(@RequestBody DeleteTaskInput deleteTaskInput) {
        return taskService.deleteTask(deleteTaskInput);
    }
}
