package levan.bujiashvili.davaleba2.dto.task;

import lombok.Data;

@Data
public class DeleteTaskOutput {
    private String msg;
}
