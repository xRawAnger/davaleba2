package levan.bujiashvili.davaleba2.dto.task;

import lombok.Data;

import java.util.List;
@Data
public class GetTasksOutput {
    private List<TaskDTO> tasks;
}
